﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shapes : MonoBehaviour
{

	// Use this for initialization
	void Start ()
    {
        A a = new A();
        a.log();

        B b = new B();
        b.log();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}

class A
{
    public virtual void log()
    {
        Debug.Log("IO SONO A!!");
    }
}

class B : A
{
    public override void log()
    {
        Debug.Log("IO SONO B!!");
    }
}
