﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ghost : MonoBehaviour
{
    static public int Points = 200;

    
    GameManager _gameManager;
    NavMeshAgent _navAgent;
    GameObject _player;
    PacmanMovement _pacman;


    [SerializeField]
    GameObject ghostMesh;

    [SerializeField]
    GameObject EyesMesh;

    [SerializeField]
    Transform SpawnPoint;

    [SerializeField]
    Material DefaultMaterial;

    [SerializeField]
    Material AfraidMaterial;

    Renderer _ghostMeshRenderer;

    bool _isDead;

    Collider _collider;

    void Awake()
    {
        _gameManager = GameObject.FindObjectOfType<GameManager>();
        _collider = gameObject.GetComponent<Collider>();
        _navAgent = GetComponent<NavMeshAgent>();

        _ghostMeshRenderer = ghostMesh.GetComponent<Renderer>();
        DefaultMaterial = _ghostMeshRenderer.material;
    }

    //void OnDisable()
    //{
    //    Destroy(ghostMesh);
    //}

    // Use this for initialization
    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        _pacman = _player.GetComponent<PacmanMovement>();
        EyesMesh.SetActive(false);

        //Debug.Log("TOTALE PILLOLE=" + Pill.GetTotalPillCount());

        if (SpawnPoint == null)
            throw new UnityException("Spawn point is missing");

    }

    // Update is called once per frame
    void Update()
    {
        if (_gameManager.IsGameReady() == false)
            return;

        ghostMesh.transform.position = transform.position;
        EyesMesh.transform.position = transform.position;


        if (!_isDead) //ghost alive
        {
            _navAgent.SetDestination(_player.transform.position);

            SetMaterial();
        }
        else //ghost dead
        {

            if (isInsideSpawn())
            {
                //respawn
                Respawn();
            }
        }


    }

    private void Respawn()
    {
        ghostMesh.SetActive(true);
        EyesMesh.SetActive(false);
        _isDead = false;
        _collider.enabled = true;

        SetMaterial();
    }

    private void SetMaterial()
    {
        if (_pacman.HasPowerUp())
        {
            _ghostMeshRenderer.material = AfraidMaterial;
        }
        else
        {
            _ghostMeshRenderer.material = DefaultMaterial;
        }
    }

    bool isInsideSpawn()
    {
        return (gameObject.transform.position - SpawnPoint.transform.position).magnitude <= 0.5;
    }
    public void OnDeath()
    {
        _isDead = true;
        ghostMesh.SetActive(false);
        EyesMesh.SetActive(true);
        _navAgent.SetDestination(SpawnPoint.position);
        _collider.enabled = false;
    }
}
