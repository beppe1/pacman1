﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    PacmanMovement _pacman;

    AudioSource _audioSource;

    [SerializeField]
    GameObject _gameOverUI;

    [SerializeField]
    AudioClip _audioStart;

    bool _isGameReady;
    public bool IsGameReady()
    {
        return _isGameReady;
    }

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start()
    {

        if (_pacman == null)
            throw new UnityException("Pacman is missing!");

        _gameOverUI.SetActive(false);

        StartCoroutine(CheckStartMusic());
    }

    // Update is called once per frame
    void Update()
    {
        if (CheckDefeat())
        {
            OnDefeat();
        }
        else if (CheckVictory())
        {
            OnVictory();
        }
    }

    void OnVictory()
    {
        Debug.Log("HAI VNTO!");
        SceneManager.LoadScene("stage02");
    }

    void OnDefeat()
    {
        _gameOverUI.SetActive(true);
        _isGameReady = false;
    }


    /// <summary>
    /// Controlla la condione di vittoria.
    /// </summary>
    /// <returns>True se il giocatore ha vinto, altrimenti false.</returns>
    bool CheckVictory()
    {
        return (_pacman.GetEatenPills() == Pill.GetTotalPillCount());
    }

    /// <summary>
    /// Controlla la conizione di sconfitta. (Quando il giocatore arriva a zero vite).
    /// </summary>
    /// <returns></returns>
    bool CheckDefeat()
    {
        return (_pacman.GetLives() == 0);
    }

    /// <summary>
    /// Aspetta che la musica di inizio sia terminata prima di permettere l'avvio del gioco.
    /// </summary>
    /// <returns></returns>
    IEnumerator CheckStartMusic()
    {
        _isGameReady = false;
        _audioSource.PlayOneShot(_audioStart);
        while (_audioSource.isPlaying)
            yield return null;
        _isGameReady = true;
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene("Pacman");
    }
}


